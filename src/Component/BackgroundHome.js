import React from "react"
import SearchBar from "../Component/SearchBar"
import Banner from "../Component/Banner"
import Category from "../Component/Category"
import Rekomendasi from "../Component/Rekomendasi"
import Button from "../Component/Button"


function BackgroundHome() {
    return(
        <div style ={{
            backgroundColor:"#e52346",
            backgroundSize:"cover",
            backgroundRepeat:"no-repeat",
            border:"none",
            display:"block",
            height:"55px",
            width:"100%",
            position:"fixed",
            top:"-1px"

        }}>
           
<SearchBar/>
<Banner/>
<Category/>
<Rekomendasi/>
<Button/>
        </div>

    );
    
}
export default BackgroundHome;