import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import SwipeableViews from "react-swipeable-views";
import { autoPlay } from "react-swipeable-views-utils";
import Grid from "@material-ui/core/Grid";

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const Baner = [
  {
    imgPath:
      "https://belivery.id/wp-content/uploads/2020/06/hr6ertyry.png"
  },
  {
    imgPath:
      "https://belivery.id/wp-content/uploads/2020/05/ewslider-4.png"
  },
  {
    imgPath:
     "https://belivery.id/wp-content/uploads/2020/06/Gosend.png"
  }
];

const useStyles = makeStyles((theme) => ({
  img: {
    maxWidth:"100%",
    height: "auto",
    display: "block",
    borderRadius: "10px",
    margin: "20px"
  }
}));

function SwipeableTextMobileStepper() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);

  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  return (
    <Grid container justify="center" alignItems="center">
      <div>
        <AutoPlaySwipeableViews
          style={{
            borderRadius: "5px",
            marginLeft: "10px"
          }}
          index={activeStep}
          onChangeIndex={handleStepChange}
        >
          {Baner.map((step, index) => (
            <div key={step.label}>
              {Math.abs(activeStep - index) <= 2 ? (
                <img
                  className={classes.img}
                  src={step.imgPath}
                  alt={step.label}
                />
              ) : null}
            </div>
          ))}
        </AutoPlaySwipeableViews>
      </div>
    </Grid>
  );
}

export default SwipeableTextMobileStepper;
