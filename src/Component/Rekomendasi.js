import React, { useState, useEffect } from 'react';
import axios from "axios";
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
    marginRight: "10px",
    justifyContent: "center"
    
  },
  img: {
    width:70,
    height:70,
    marginLeft: "10px",
    justifyContent: "center"
    
  },
  text: {
    fontFamily: "Montserrat",
    fontSize: "12px",
    marginLeft: "10px"
  }
}));

export default function CenteredGrid() {
  const classes = useStyles();
  const [products, setProducts] = useState([])
   useEffect(() => {
       axios
         .get("https://belivery.id/wp-admin/admin-ajax.php?nonce=80a806851b&action=notifku_data")
         .then(response => setProducts(response.data));
     }, [])

  return (
      
    <div className={classes.root}>
      {products.slice(0,5).map((product) => (
              <Grid container spacing={3}>
        <Grid item xs={3}>
          <img className={classes.img} src={product.images[0].src}/>
        </Grid>
        <Grid item xs={6}>
        <p className={classes.text}>
          {product.name}
        </p><br/>
        <p className={classes.text}>
          <font color="red">Rp.{product.price}</font>
          <font color="#C7C7C9">/{product.meta_data[0].value}</font>
        </p>
        <Grid>
          
          <Grid img container justify="center" alignItems="center">
            
        <Grid item xs={3}>
        <Grid style={{ display: "flex", alignItems: "flex-end" }}>
       
            </Grid>
            </Grid>
        </Grid>
        
      </Grid>
      </Grid>  
      </Grid>
      ))}
    </div>
  );
}