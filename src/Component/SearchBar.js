import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import InputBase from "@material-ui/core/InputBase";
import IconButton from "@material-ui/core/IconButton";


const useStyles = makeStyles((theme) => ({
  root: {
    width:"90%",
    height: "37px",
    lineHeight: "38px",
    margin: "10px",
    boxSizing: "borderbox",
    borderRadius: "5px",
    marginLeft: "70px"
   
   
  
  },
  input: {
  marginLeft: theme.spacing(1)
  },
  img:{
    maxWidth:"100%",
    color:"#e52346",
    borderStyle:"none"
  } 
}));

export default function SearchBar() {
  const classes = useStyles();
  return (
   
    
      <Paper className={classes.root}>
        <IconButton
          type="submit"
          className={classes.iconButton}
          aria-label="search"
        > 
        </IconButton>
        <InputBase className={classes.input} placeholder="Search"/>
      </Paper>  
     
      
  );
}
